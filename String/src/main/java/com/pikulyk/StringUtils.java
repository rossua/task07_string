package com.pikulyk;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

  private List<Object> objects = new ArrayList<>();

  public StringUtils addToParameters(Object object) {
    objects.add(object);
    return this;
  }

  public String concat() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Object obj : objects) {
      stringBuilder.append(obj).append(" ");
    }
    return stringBuilder.toString();
  }
}
