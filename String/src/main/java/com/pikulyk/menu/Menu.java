package com.pikulyk.menu;

import com.pikulyk.StringUtils;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {

  private Map<String, String> menu;
  private Map<String, Printable> menuMethods;
  private static Scanner scanner = new Scanner(System.in);

  Locale locale;
  ResourceBundle bundle;

  public Menu() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("Menu", locale);
    setMenu();
    setMenuMethods();
  }

  public void setMenu() {
    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("Q", bundle.getString("Q"));
  }

  public void setMenuMethods() {
    menuMethods = new LinkedHashMap<>();
    menuMethods.put("1", this::testStringUtils);
    menuMethods.put("2" , this::getUkrainianMenu);
    menuMethods.put("3" , this::getEnglishMenu);
    menuMethods.put("4" , this::testRegEx);
    menuMethods.put("5" , this::replaceAllVowels);
    menuMethods.put("6", this::splitString);
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point");
      keyMenu = scanner.nextLine().toUpperCase();
      try {
        menuMethods.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String key : menu.keySet()) {
      System.out.println(menu.get(key));
    }
  }

  private void testStringUtils() {
    StringUtils utils = new StringUtils();
    utils.addToParameters(2018)
        .addToParameters(04.12)
        .addToParameters("Pikulyk")
        .addToParameters("Rostyslav");
    System.out.println(utils.concat());
  }

  private void getUkrainianMenu() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("Menu", locale);
    setMenu();
    show();
  }

  private void getEnglishMenu() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("Menu", locale);
    setMenu();
    show();
  }

  private void testRegEx() {
    System.out.println("Enter some sentence: ");
    String someMessage = scanner.nextLine();
    Pattern pattern = Pattern.compile("[A-Z]+[^.]*\\.+");
    Matcher matcher = pattern.matcher(someMessage);
    System.out.println(matcher.matches());
  }


  private void replaceAllVowels() {
    System.out.println("Enter some sentence in which you want to replace all vowels: ");
    String input = scanner.nextLine();
    String[] vowels = {"A", "E", "I", "O", "U", "a", "e", "i", "o", "u"};
    for (String vowel : vowels) {
      input = input.replaceAll(vowel, "_");
    }
    System.out.println(input);
  }

  private void splitString() {
    System.out.println("Enter some sentence you want to split: ");
    String input = scanner.nextLine();
    String[] arrStrings = input.split(" on ");
    for (String str : arrStrings) {
      System.out.println(str);
    }
  }
}
