package com.pikulyk.text_processing;

public class PunctuationMark {
  private char punctuationMark;

  public PunctuationMark(char punctuationMark) {
    this.punctuationMark = punctuationMark;
  }

  public char getPunctuationMark() {
    return punctuationMark;
  }
}
