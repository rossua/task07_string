package com.pikulyk.text_processing;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
  private String sentence;
  private List<Word> words;
  private List<PunctuationMark> marks;

  public Sentence(String sentence) {
    this.sentence = sentence;
    divideOnWords();
    findMarks();
  }

  public String getSentence() {
    return sentence;
  }

  public List<Word> getWords() {
    return words;
  }

  public List<PunctuationMark> getMarks() {
    return marks;
  }

  private void divideOnWords() {
    words = new ArrayList<>();
    String[] arr = sentence.split("[\\s(-.]+");
    for (int i = 0; i < arr.length; i++) {
      words.add(new Word(arr[i]));
    }
  }

  private void findMarks() {
    marks = new ArrayList<>();
    Pattern pattern = Pattern.compile("[,.\\-();:\"\']");
    Matcher matcher = pattern.matcher(sentence);
    while (matcher.find()) {
      marks.add(new PunctuationMark(sentence.charAt(matcher.start())));
    }
  }
}
