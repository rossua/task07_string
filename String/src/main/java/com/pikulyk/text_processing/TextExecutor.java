package com.pikulyk.text_processing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextExecutor {

  private String allText;
  private List<Sentence> sentences;
  private static final String PATH_TO_FILE
      = "pass to file";
  private static final int MAX_WORD_AMOUNT = 40;

  public TextExecutor() {
    this.allText = readFromFile();
    divideOnSentences();
  }

  private String readFromFile() {
    StringBuilder stringBuilder = new StringBuilder();
    try {
      String line;
      BufferedReader input = new BufferedReader(new FileReader(PATH_TO_FILE));
      while ((line = input.readLine()) != null) {
        stringBuilder.append(line).append(System.lineSeparator());
      }
    } catch (Exception e) {
      System.out.println("File not found");
      e.printStackTrace();
    }
    return stringBuilder.toString();
  }

  private void divideOnSentences() {
    sentences = new ArrayList<>();
    Pattern p = Pattern.compile("[\\.?!]");
    Matcher m = p.matcher(allText);
    int start = 0;
    while (m.find()) {
      sentences.add((new Sentence(allText.substring(start, m.start() + 1).trim())));
      start = m.end();
    }
//    String[] arr = allText.split("[\\.?!]");
//    for (int i = 0; i < arr.length; i++) {
//      this.sentences.add(new Sentence(arr[i]));
//    }
  }

  private int amountOfSentencesWithRepetedWords() {
    System.out.println("Amount of sentences with repeted words: ");
    int result = 0;
    for (int i = 0; i < sentences.size(); i++) {
      int amount = 0;
      for (int j = 0; j < sentences.get(i).getWords().size(); j++) {
        for (int k = 0; k < sentences.get(i).getWords().size(); k++) {
          if ((sentences.get(i).getWords().get(j).getWord()
              .equalsIgnoreCase(sentences.get(i).getWords().get(k).getWord())) && j != k) {
            amount++;
            break;
          }
        }
      }
      if (amount > 0) {
        result++;
      }
    }
    return result;
  }

  private void printSentencesAscending() {
    int counter = 0;
    System.out.println("Sentences in ascending order: ");
    for (int i = 2; i < MAX_WORD_AMOUNT; i++) {
      for (int j = 0; j < sentences.size(); j++) {
        if (sentences.get(j).getWords().size() == i) {
          System.out.println(++counter + ". " + sentences.get(j).getSentence());
        }
      }
    }
  }

  private void findUniqeWordInFirstSentence() {
    boolean isUniqe;

    for (int i = 0; i < sentences.get(0).getWords().size(); i++) {
      isUniqe = true;
      for (int j = 1; j < sentences.size(); j++) {
        for (int k = 0; k < sentences.get(j).getWords().size(); k++) {
          if ((sentences.get(0).getWords().get(i).getWord()
              .equalsIgnoreCase(sentences.get(j).getWords().get(k).getWord()))) {
            isUniqe = false;
            break;
          }
        }
        if (!isUniqe) {
          break;
        }
      }
      if (isUniqe) {
        System.out.println("Uniqe word: " + sentences.get(0).getWords().get(i).getWord());
      } else if (sentences.get(0).getWords().size() - 1 == i) {
        System.out.println("There is no uniqe word in first sentence (");
      }
    }
  }
}
